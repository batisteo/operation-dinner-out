.PHONY: all

all : install run runserver test check

install :
	poetry config virtualenvs.in-project true
	poetry install
	poetry run pre-commit install
	docker-compose build .

run :
	./start.sh

runserver :
	poetry run python manage.py runserver

test :
	docker-compose run app pytest

check :
	poetry run pre-commit run -a