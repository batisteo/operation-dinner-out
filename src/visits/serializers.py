from statistics import mean

from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from . import models

User = get_user_model()


def average(field: str, queryset):
    return mean(
        getattr(obj, field) for obj in queryset if getattr(obj, field) is not None
    )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "username"]


class VisitSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Visit
        fields = [
            "date",
            "restaurant",
            "rating",
            "expense",
            "note",
        ]


class RestaurantSerializer(GeoFeatureModelSerializer):
    visits = serializers.SerializerMethodField()

    class Meta:
        model = models.Restaurant
        geo_field = "place"
        fields = ["name", "cuisine", "place", "visits"]

    def get_visits(self, restaurant):
        user = self.context["request"].user
        visits = restaurant.visits.filter(user=user)
        return {
            "dates": sorted(v.date for v in visits),
            "average_rating": average("rating", visits),
            "average_expense": average("expense", visits),
        }


class CuisineSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Cuisine
        fields = ["url", "name"]
