from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class Rating(models.IntegerChoices):
    BAD = 1
    MEH = 2
    OK = 3
    GREAT = 4
    EXCELLENT = 5


class Visit(models.Model):

    date = models.DateTimeField(_("time"), auto_now=False, auto_now_add=False)
    restaurant = models.ForeignKey(
        "visits.Restaurant",
        verbose_name=_("restaurant"),
        related_name="visits",
        on_delete=models.PROTECT,
    )
    rating = models.PositiveSmallIntegerField(_("rating"), choices=Rating.choices)
    expense = models.DecimalField(
        _("expense"), max_digits=5, decimal_places=2, blank=True, null=True
    )
    note = models.TextField(_("note"), blank=True)
    user = models.ForeignKey(
        User, verbose_name=_("user"), on_delete=models.SET_NULL, null=True, blank=True
    )

    class Meta:
        verbose_name = _("visit")
        verbose_name_plural = _("visits")

    def __str__(self):
        return f"{self.date} at {self.restaurant}"


class Restaurant(models.Model):

    name = models.CharField(_("name"), max_length=50)
    cuisine = models.ForeignKey(
        "visits.Cuisine",
        verbose_name=_("cuisine"),
        related_name="restaurants",
        on_delete=models.SET_NULL,
        null=True,
    )
    place = models.PointField(_("place"))
    created_by = models.ForeignKey(
        User,
        verbose_name=_("created by"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _("restaurant")
        verbose_name_plural = _("restaurants")

    def __str__(self):
        return self.name


class Cuisine(models.Model):

    name = models.CharField(_("name"), max_length=50)
    created_by = models.ForeignKey(
        User,
        verbose_name=_("created by"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _("cuisine")
        verbose_name_plural = _("cuisines")

    def __str__(self):
        return self.name
