from datetime import datetime


def test_cuisine_str(cuisine_factory):
    cuisine = cuisine_factory(name="Česky")
    assert str(cuisine) == "Česky"


def test_restaurant_str(restaurant_factory):
    restaurant = restaurant_factory(name="Lokál")
    assert str(restaurant) == restaurant.name


def test_visit_str(visit_factory, restaurant_factory):
    now = datetime.utcnow()
    restaurant = restaurant_factory(name="Lokál")
    visit = visit_factory(date=now, restaurant=restaurant)
    assert str(visit) == f"{now} at {restaurant}"
