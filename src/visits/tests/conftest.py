import pytest
from django_factories import Factory

from ..models import Cuisine, Restaurant, Visit


@pytest.fixture
def cuisine_factory(request):
    factory = Factory(Cuisine)
    return factory(request)


@pytest.fixture
def restaurant_factory(request):
    factory = Factory(Restaurant)
    return factory(request)


@pytest.fixture
def visit_factory(request):
    factory = Factory(Visit)
    return factory(request)
