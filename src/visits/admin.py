from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin

from .models import Cuisine, Restaurant, Visit


@admin.register(Visit)
class VisitAdmin(OSMGeoAdmin):
    list_display = ["date", "restaurant"]
    fields = [
        "date",
        "restaurant",
        "rating",
        "expense",
        "note",
    ]

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.user = request.user
        super().save_model(request, obj, form, change)


@admin.register(Restaurant)
class RestaurantAdmin(admin.ModelAdmin):
    list_display = ["name", "cuisine"]
    fields = [
        "name",
        "cuisine",
        "place",
    ]

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(Cuisine)
class CuisineAdmin(admin.ModelAdmin):
    fields = [
        "url",
        "name",
    ]

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)
