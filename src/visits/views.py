from django.contrib.auth import get_user_model
from rest_framework import mixins, permissions, status, viewsets
from rest_framework.response import Response

from .models import Cuisine, Restaurant, Visit
from .serializers import (
    CuisineSerializer,
    RestaurantSerializer,
    UserSerializer,
    VisitSerializer,
)

User = get_user_model()


class CreateListGetViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    pass


class UserViewSet(CreateListGetViewSet):
    """
    API endpoint that allows visits to be viewed or edited.
    """

    class Meta:
        model = User

    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class VisitViewSet(CreateListGetViewSet):
    """
    API endpoint that allows visits to be viewed or edited.
    """

    class Meta:
        model = Visit

    serializer_class = VisitSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return Visit.objects.filter(user=self.request.user)

    def create(self, request):
        resto_id = request.data["restaurant"]
        try:
            Restaurant.objects.get(pk=int(resto_id), created_by=request.user.pk)
        except Restaurant.DoesNotExist:
            return Response(
                {"error": f"User {request.user} did not create Restaurant {resto_id}"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return super().create(request)


class RestaurantViewSet(CreateListGetViewSet):
    """
    API endpoint that allows restaurants to be viewed or edited.
    """

    class Meta:
        model = Restaurant

    serializer_class = RestaurantSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return Restaurant.objects.filter(created_by=self.request.user).prefetch_related(
            "visits"
        )


class CuisineViewSet(CreateListGetViewSet):
    """
    API endpoint that allows cuisines to be viewed or edited.
    """

    class Meta:
        model = Cuisine

    serializer_class = CuisineSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return Cuisine.objects.filter(created_by=self.request.user)
