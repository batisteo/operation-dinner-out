from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions, routers
from rest_framework_simplejwt.views import (
    token_obtain_pair,
    token_refresh,
    token_verify,
)

from visits import views

from .users import UserPasswordResetForm

schema_view = get_schema_view(
    openapi.Info(
        title="Operation Dinner Out",
        default_version="v1",
        description="Rate your favorite restaurants",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="baptiste.darthenay@gmail.com"),
        license=openapi.License(name="WTFPL License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

router = routers.DefaultRouter()
router.register(r"visits", views.VisitViewSet, basename="visit")
router.register(r"restaurants", views.RestaurantViewSet, basename="restaurant")
router.register(r"cuisines", views.CuisineViewSet, basename="cuisine")
router.register(r"users", views.UserViewSet, basename="user")

auth_patterns = [
    path(
        "reset-password/",
        auth_views.PasswordResetView.as_view(form_class=UserPasswordResetForm),
        name="password_reset",
    ),
    path(
        "reset-password/done/",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
]

urlpatterns = [
    path("admin/", admin.site.urls),
    path("accounts/", include(auth_patterns)),
    path("api/token/refresh/", token_refresh, name="token_refresh"),
    path("api/token/verify/", token_verify, name="token_verify"),
    path("api/token/", token_obtain_pair, name="token_obtain_pair"),
    path("api/", include(router.urls)),
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]
