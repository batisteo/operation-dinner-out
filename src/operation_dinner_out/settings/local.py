from os import environ

from .base import *  # noqa

DEBUG = True

SECRET_KEY = environ.get("DJANGO_SECRET_KEY", "n0_k3y")

DATABASES["default"].update(  # noqa: F405
    {
        "NAME": environ.get("POSTGRES_NAME", "dinnerout"),
        "USER": environ.get("POSTGRES_USER", "dinnerout"),
        "PASSWORD": environ.get("POSTGRES_PASSWORD", "dinnerout"),
        "HOST": environ.get("POSTGRES_HOST", "127.0.0.1"),  # db for Docker
        "PORT": environ.get("POSTGRES_PORT", "5432"),
    }
)

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
