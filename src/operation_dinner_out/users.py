from celery import shared_task
from django.contrib.auth.forms import PasswordResetForm
from django.core.mail import EmailMultiAlternatives
from django.template import loader


@shared_task
def send_mail_later(subject, body, from_email, recipients, html_email):
    email_message = EmailMultiAlternatives(subject, body, from_email, recipients)
    if html_email is not None:
        email_message.attach_alternative(html_email, "text/html")
    email_message.send()


class UserPasswordResetForm(PasswordResetForm):
    def send_mail(
        self,
        subject_template_name,
        email_template_name,
        context,
        from_email,
        to_email,
        html_email_template_name=None,
    ):
        """
        Send a django.core.mail.EmailMultiAlternatives to `to_email`.
        """
        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = "".join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)
        html_email = (
            None
            if html_email_template_name is None
            else loader.render_to_string(html_email_template_name, context)
        )

        send_mail_later.delay(
            subject=subject,
            body=body,
            from_email=from_email,
            recipients=[to_email],
            html_email=html_email,
        )
