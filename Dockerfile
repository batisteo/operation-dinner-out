# syntax = docker/dockerfile:1.2
FROM python:3.9-slim

ARG DJANGO_SETTINGS_MODULE=operation_dinner_out.settings.local

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV DJANGO_SETTINGS_MODULE="${DJANGO_SETTINGS_MODULE}"


WORKDIR /code

COPY pyproject.toml poetry.lock ./

RUN --mount=type=cache,target=/var/lib/apt \
    --mount=type=cache,target=/var/cache/apt \
    --mount=type=cache,target=~/.cache/pypoetry/cache \
    --mount=type=cache,target=~/.cache/pypoetry/artifacts \
    apt update && \
    apt install -y --no-install-recommends build-essential libgdal-dev && \
    pip install --no-cache-dir poetry==1.1.5 && \
    poetry config virtualenvs.create false && \
    poetry install -v && \
    rm -r ~/.cache/pypoetry && \
    apt remove -y build-essential && \
    apt autoremove -y && \
    apt clean autoclean && \
    rm -rf /var/lib/apt/lists/*

COPY . ./

RUN poetry config virtualenvs.create false && \
    poetry install -v && \
    poetry run python manage.py check

EXPOSE 8000
